<?php declare(strict_types=1);
/*
 * irstea/pcov-clobber-plugin - Automatically apply pcov/clobber when need be.
 * Copyright (C) 2019-2020 IRSTEA
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Irstea\PcovClobberPlugin;

use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Composer\Script\Event;
use Composer\Script\ScriptEvents;
use Composer\Util\ProcessExecutor;

final class PcovClobberPlugin implements PluginInterface, EventSubscriberInterface
{
    public function activate(Composer $composer, IOInterface $io)
    {
        // NOOP
    }

    public function deactivate(Composer $composer, IOInterface $io)
    {
       // NOOP
    }

    public function uninstall(Composer $composer, IOInterface $io)
    {
        // NOOP
    }

    public static function getSubscribedEvents()
    {
        return [
            ScriptEvents::POST_AUTOLOAD_DUMP => ['onPostAutoloadDump', 1],
        ];
    }

    public function onPostAutoloadDump(Event $event)
    {
        $config = $event->getComposer()->getConfig();
        $cmdPath = escapeshellcmd($config->get('bin-dir') . \DIRECTORY_SEPARATOR . 'pcov');

        $configSourceName = $event->getComposer()->getConfig()->getConfigSource()->getName();
        if (!file_exists($configSourceName)) {
            $event->getIO()->write("PcovClobberPlugin: could not guess project path from `$configSourceName`", true, IOInterface::QUIET);

            return;
        }
        $projectPath = \dirname($configSourceName);

        $cmd = "$cmdPath . clobber";
        $event->getIO()->write("PcovClobberPlugin: executing $cmd");

        $output = null;
        $exitCode = (new ProcessExecutor($event->getIO()))->execute($cmd, $output, $projectPath);
        if ($exitCode !== 0) {
            $event->getIO()->write("PcovClobberPlugin: command has exited with code $exitCode");
        }
    }
}

